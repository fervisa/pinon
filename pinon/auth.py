from functools import wraps
from flask import request, Response
from pinon.db import get_db, query_db
import bcrypt

def check_auth(username, password):
    """Validate credentials included in the request"""
    user = query_db('select * from users where username = ?', [username], one=True)
    if user is None:
        return False
    else:
        return check_password(password, user['password'])

def please_authenticate():
    """Build response body asking for authentication with a 401 response code"""
    return Response(
        'No se pudieron verificar credenciales para acceder a esta URL',
        401, { 'WWW-Authenticate': 'Basic realm="Login Required"' }
    )

def requires_auth(f):
    """Decorator function used to wrap 'view' functions
    in order to require authentication

    @requires_auth
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return please_authenticate()
        return f(*args, **kwargs)
    return decorated

def create_user(username, password):
    """Create new user.
    This new user will have access to all protected resources
    (Basic HTTP Auth)
    """
    db = get_db()
    enc_password = generate_password(password)
    db.execute('insert into users (username, password) values (?, ?)',
            [username, enc_password])
    db.commit()

def generate_password(password):
    return bcrypt.hashpw(password, bcrypt.gensalt())

def check_password(raw_password, enc_password):
    return bcrypt.checkpw(raw_password, enc_password)
