from flask import Flask, render_template, g
from pinon.auth import requires_auth, create_user
from pinon.db import get_db
import click

app = Flask(__name__)

def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

""""Command line tools"""

@app.cli.command('initdb')
def initdb_command():
    """Initialize the database."""
    init_db()
    print('Initialized the database.')

@app.cli.command('createuser')
@click.option('--username', prompt='your username')
@click.option('--password', prompt='your password', hide_input=True, confirmation_prompt=True)
def createuser_command(username, password):
    """Create new user in the database."""
    create_user(username, password)
    print('User added to the database.')

""""Application HTTP endpoints"""

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/pinon')
@requires_auth
def pinon():
    # codigo de pinon.py se copia aqui
    return 'Pin ON'

@app.route('/pinoff')
@requires_auth
def pinoff():
    # codigo de pinoff.py se copia aqui
    return 'Pin OFF'

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
