drop table if exists users;
create table users(
  id integer primary key autoincrement,
  username varchar(50),
  password varchar(254)
);
