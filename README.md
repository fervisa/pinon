# README #

### How do I get set up? ###

#### Install the app ####

1. Download the code from this repo
2. (Recommended) Create Python virtual env
	```python3 -m venv venv```
3. Use newly created virtual env
	```. venv/bin/activate```
4. Install project as a package
	```pip install --editable .```
5. Launch the web server
	```FLASK_APP=pinon flask run --host=0.0.0.0```

#### Initialize the DB ####

1. Build the DB
	```FLASK_APP=pinon flask initdb```
2. Create a new user, this user will have access to all HTTP protected endpoints
	```FLASK_APP=pinon flask createuser```