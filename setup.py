from setuptools import setup

setup(
    name='pinon',
    packages=['pinon'],
    include_package_data=True,
    install_requires=[
        'flask', 'py-bcrypt'
    ],
)
